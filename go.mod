module gitlab.com/jhord/marker

go 1.13

require (
	github.com/alecthomas/chroma v0.6.7
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/gomarkdown/markdown v0.0.0-20190912180731-281270bc6d83
)
